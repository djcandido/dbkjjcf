<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PruebaTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_resultado_paises(): void
    {
        $response = $this->get('/api/obtenerpaises');
        //dd($response);
        //$response->assertStatus(200);
        //$response->assertJsonCount(3);
        /*$response->assertJsonStructure([
            ["id_pais",
            "nombre_pais"]
        ]);*/
        $response->assertSee("Noruega");
    }
}
